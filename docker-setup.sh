#!/bin/bash

# build the app container
docker build -t  kolomytsev/laravel .


# create the network
docker network create laravel-app

# start the mysql container
docker run -d --net laravel-app -p 3382:3306 --env="MYSQL_ROOT_PASSWORD=[fcnbyfgeh" --name db mysql


# start the laravel app container
docker run --net laravel-app -p 8082:80 -v $PWD:/var/www -d --name laravel kolomytsev/laravel 


# create database
echo "PASSWORD TO DATABASE"
echo "CREATE DATABASE laravel" | mysql -h 127.0.0.1 -u root -p -P 3382