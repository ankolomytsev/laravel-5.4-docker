#!/bin/bash

docker stop db
docker stop laravel
docker rm db
docker rm laravel
docker network rm laravel-app
